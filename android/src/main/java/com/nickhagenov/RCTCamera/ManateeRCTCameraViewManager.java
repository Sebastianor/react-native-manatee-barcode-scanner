package com.nickhagenov.RCTCamera;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.*;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.List;
import java.util.ArrayList;

public class ManateeRCTCameraViewManager extends ViewGroupManager<ManateeRCTCameraView> {
    private static final String REACT_CLASS = "ManateeRCTCamera";
    private ManateeRCTCameraView instance = null;
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public ManateeRCTCameraView createViewInstance(ThemedReactContext context) {
        return new ManateeRCTCameraView(context);
    }

    @ReactProp(name = "aspect")
    public void setAspect(ManateeRCTCameraView view, int aspect) {
        view.setAspect(aspect);
    }

    @ReactProp(name = "captureMode")
    public void setCaptureMode(ManateeRCTCameraView view, final int captureMode) {
        // Note that this in practice only performs any additional setup necessary for each mode;
        // the actual indication to capture a still or record a video when capture() is called is
        // still ultimately decided upon by what it in the options sent to capture().
        view.setCaptureMode(captureMode);
    }

    @ReactProp(name = "captureTarget")
    public void setCaptureTarget(ManateeRCTCameraView view, int captureTarget) {
        // No reason to handle this props value here since it's passed again to the ManateeRCTCameraModule capture method
    }

    @ReactProp(name = "type")
    public void setType(ManateeRCTCameraView view, int type) {
        view.setCameraType(type);
    }

    @ReactProp(name = "captureQuality")
    public void setCaptureQuality(ManateeRCTCameraView view, String captureQuality) {
        view.setCaptureQuality(captureQuality);
    }

    @ReactProp(name = "torchMode")
    public void setTorchMode(ManateeRCTCameraView view, int torchMode) {
        view.setTorchMode(torchMode);
    }

    @ReactProp(name = "flashMode")
    public void setFlashMode(ManateeRCTCameraView view, int flashMode) {
        view.setFlashMode(flashMode);
    }

    @ReactProp(name = "orientation")
    public void setOrientation(ManateeRCTCameraView view, int orientation) {
        view.setOrientation(orientation);
    }

    @ReactProp(name = "captureAudio")
    public void setCaptureAudio(ManateeRCTCameraView view, boolean captureAudio) {
        // TODO - implement video mode
    }

    @ReactProp(name = "barcodeScannerEnabled")
    public void setBarcodeScannerEnabled(ManateeRCTCameraView view, boolean barcodeScannerEnabled) {
        view.setBarcodeScannerEnabled(barcodeScannerEnabled);
    }

    @ReactProp(name = "barCodeTypes")
    public void setBarCodeTypes(ManateeRCTCameraView view, ReadableArray barCodeTypes) {
        if (barCodeTypes == null) {
            return;
        }
        List<String> result = new ArrayList<String>(barCodeTypes.size());
        for (int i = 0; i < barCodeTypes.size(); i++) {
            result.add(barCodeTypes.getString(i));
        }
        ManateeScanner.setBarcodeTypes(result);
    }

    @ReactProp(name = "manateeKey")
    public void setManateeKey(ManateeRCTCameraView view, String key) {
        ManateeScanner.setKey(key);
    }
}
