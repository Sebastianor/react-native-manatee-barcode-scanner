package com.nickhagenov.RCTCamera;

import java.util.Collections;
import java.util.List;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;

public class ManateeRCTCameraPackage implements ReactPackage {
    private static ManateeRCTCameraModule instance = null;

    public static ManateeRCTCameraModule getModuleInstance() {
        return instance;
    }
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactApplicationContext) {
        instance = new ManateeRCTCameraModule(reactApplicationContext);
        return Collections.<NativeModule>singletonList(instance);
    }

    public List<Class<? extends JavaScriptModule>> createJSModules() {
        return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactApplicationContext) {
        //noinspection ArraysAsListWithZeroOrOneArgument
        return Collections.<ViewManager>singletonList(new ManateeRCTCameraViewManager());
    }

}
